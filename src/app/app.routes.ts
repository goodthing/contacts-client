import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ViewContactsComponent } from './contacts/view-contacts/view-contacts.component';
import { MaintainContactsComponent } from './contacts/maintain-contacts/maintain-contacts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'view-contacts', component: ViewContactsComponent },
    { path: 'maintain-contacts/:id', component: MaintainContactsComponent },
    {path:'not-found', component:PageNotFoundComponent},
    { path: '**', redirectTo: '/not-found' }
];