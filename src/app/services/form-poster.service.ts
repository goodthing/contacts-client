import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Contact } from '../models/contact.model';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FormPosterService {
  url = 'http://localhost:54344/api/contacts';

  constructor(private http: Http) { }

  handleError(error: any): any {
    console.log('Post error: ', error);

    return Observable.throw(error.statusText);
  }

  extractData(res: Response): any {
    let body = res.json();

    return body.fields || {};
  }

  postContact(contact: Contact): Observable<any> {
    let body = JSON.stringify(contact);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.url, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  putContact(contact: Contact, id: string): Observable<any> {
    let body = JSON.stringify(contact);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let url = this.url + '/' + id;

    return this.http.put(url, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
}
