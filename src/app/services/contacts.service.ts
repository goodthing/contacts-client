import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ContactsService {
  getUrl = 'http://localhost:54344/api/contacts';

  constructor(private http: Http) { }

  getContacts() {
    return this.http.get(this.getUrl)
      .map(
        (response: Response) => { return response.json(); }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Could not load Contacts');
        }
      );
  }

  getSingleContact(id: string) {
    if (id !== '0') {
      let url = this.getUrl + '/' + id;

      return this.http.get(url)
        .map(
          (response: Response) => { return response.json(); }
        )
        .catch(
          (error: Response) => {
            return Observable.throw('Could not load Contact');
          }
        );
    }
  }
}
