export class Contact{
    public id:string;
    public firstName:string;
    public lastName:string;
    public email:string;
    public telephone: string;

    constructor(firstName:string,lastName:string,email:string,telephone:string,id?:string){
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
        this.telephone=telephone;
    }
}