import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../../services/contacts.service';
import { Contact } from '../../models/contact.model';

@Component({
  selector: 'app-view-contacts',
  templateUrl: './view-contacts.component.html',
  styleUrls: ['./view-contacts.component.css']
})
export class ViewContactsComponent implements OnInit {
  contacts: Contact[];
  filteredContacts: Contact[];
  _listFilter: string;
  GETMessage='Loading Contacts...';

  constructor(private contactsService: ContactsService) { }

  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredContacts = this.listFilter ?
      this.performFilter(this.listFilter) : this.contacts;
  }

  performFilter(filterBy: string): Contact[] {
    filterBy = filterBy.toLocaleLowerCase();

    return this.contacts.filter((contact: Contact) =>
      contact.firstName.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
      contact.lastName.toLocaleLowerCase().indexOf(filterBy) !== -1
    );
  }

  refreshContacts() {
    this.contactsService.getContacts()
      .subscribe(
        (contacts: Contact[]) => {
          this.contacts = contacts;
          this.filteredContacts = this.contacts;
        },
        (error) => this.GETMessage=error
      );
  }
  ngOnInit() {
    this.refreshContacts();
  }

}
