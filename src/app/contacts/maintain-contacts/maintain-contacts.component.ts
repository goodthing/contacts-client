import { Component, OnInit, ViewChild } from '@angular/core';
import { Contact } from '../../models/contact.model';
import { ContactsService } from '../../services/contacts.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FormPosterService } from '../../services/form-poster.service';

@Component({
  selector: 'app-maintain-contacts',
  templateUrl: './maintain-contacts.component.html',
  styleUrls: ['./maintain-contacts.component.css']
})
export class MaintainContactsComponent implements OnInit {
  @ViewChild('form') form: NgForm;
  model = new Contact('', '', '', '');
  contactId: string;
  heading: string;
  submitText: string;
  showNewButton: boolean;
  posting: boolean;
  backButtonText: string;
  responseText: string;
  saveSuccess: boolean = true;
  actionTaken: boolean;

  constructor(private contactsService: ContactsService,
    private formPoster: FormPosterService,
    private route: ActivatedRoute,
    private router: Router) { }

  getContact(id: string) {
    this.contactsService.getSingleContact(id).subscribe(
      (contact: Contact) => this.model = contact,
      (error) => {console.log(error);
        this.router.navigate(['/not-found'])
      }
    );
  }

  resetForm() {
    this.form.reset();
  }

  handleResponse(success: boolean) {
    this.saveSuccess = success;
    setTimeout(()=>{
      this.responseText = success ? 'Success' : 'An error occurred';
    },1000)
  }

  submitForm(form: NgForm) {
    this.actionTaken = true;
    this.responseText ='Saving...';

    if (this.posting) {
      this.formPoster.postContact(this.model)
        .subscribe(
          data => this.handleResponse(true),
          err => this.handleResponse(false)
        );
    }
    else {
      this.formPoster.putContact(this.model, this.contactId)
        .subscribe(
          data => this.handleResponse(true),
          err => this.handleResponse(false)
        );
    }
  }

  initModel(newContact: boolean) {
    this.actionTaken = false;
    this.responseText = 'Ready';
    this.heading = newContact ? 'Add New Contact' : 'Maintain Contact';
    this.submitText = newContact ? 'Add Contact' : 'Save Contact';
    this.backButtonText = newContact ? 'View List' : 'Back';
    this.showNewButton = !newContact;
    this.posting = newContact;

    newContact ? this.model = new Contact('', '', '', '')
      : this.getContact(this.contactId);
  }

  onBack() {
    this.showNewButton ?
      this.router.navigate(['/single-contact', this.contactId]) :
      this.router.navigate(['/view-contacts']);
  }

  ngOnInit() {
    this.contactId = this.route.snapshot.paramMap.get('id');
    let isNewContact = this.contactId === '0';
    
    this.initModel(isNewContact);

    this.route.params
      .subscribe(
        (params: Params) => {
          this.contactId = params['id'];
          let isNewContact = this.contactId === '0';
          
          this.initModel(isNewContact);
        }
      );
  }
}
