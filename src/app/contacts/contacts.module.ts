import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaintainContactsComponent } from './maintain-contacts/maintain-contacts.component';
import { ViewSingleContactComponent } from './view-single-contact/view-single-contact.component';
import { ViewContactsComponent } from './view-contacts/view-contacts.component';
import { ContactsService } from '../services/contacts.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormPosterService } from '../services/form-poster.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forChild([
        { path: 'view-contacts', component: ViewContactsComponent },
        { path: 'single-contact/:id',
          component: ViewSingleContactComponent }
    ]),
  ],
  declarations: [
    MaintainContactsComponent,
    ViewContactsComponent,
    ViewSingleContactComponent
  ],
  providers: [
    ContactsService,
    FormPosterService
  ]
})
export class ContactsModule { }
