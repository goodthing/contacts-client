import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../../services/contacts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '../../models/contact.model';

@Component({
  selector: 'app-view-single-contact',
  templateUrl: './view-single-contact.component.html',
  styleUrls: ['./view-single-contact.component.css']
})
export class ViewSingleContactComponent implements OnInit {
  contact:Contact;

  constructor(private contactsService: ContactsService,
      private route:ActivatedRoute,
      private router: Router) {}

  ngOnInit() {
    let id=this.route.snapshot.paramMap.get('id');

    this.contactsService.getSingleContact(id).subscribe(
      (contact: Contact)=> this.contact=contact,
      (error) => {
        this.router.navigate(['/not-found']);  
      }
    );
  }

  onBack(){
    this.router.navigate(['/view-contacts']);
  }

  onEdit(){
    this.router.navigate(['/maintain-contacts', this.contact.id]);
  }
}
